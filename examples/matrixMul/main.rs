#![allow(non_snake_case)]

use clap::Parser;
use cricket::{Client, ParameterBuilder};
use std::fs::{self, File};
use std::hint::black_box;
use std::io::Read;
use std::net::IpAddr;
use std::path::Path;
use std::str::FromStr;
use std::time::Instant;

#[cfg(target_os = "hermit")]
use hermit_sys as _;

#[path = "../common/lib.rs"]
mod common;

const BLOCKSIZE: usize = 32;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    /// Set GPU with device-number
    #[arg(long, default_value_t = 0)]
    device: i32,

    /// IP of the cricket server
    #[arg(long, short, default_value_t = String::from("127.0.0.1"), value_name = "IP")]
    cricket_server: String,

    /// Number of runs for each benchmark.
    #[arg(long, short, default_value_t = 10)]
    iterations: usize,

    /// Path to the cubin containing the kernels
    #[arg(long)]
    cubin: Option<String>,
}

// TODO: unsafe transmute shenanigans
fn vec_cast<T>(mut vec_from: Vec<T>) -> Vec<u8> {
    // Convert any vector to Vec<u8> for cuda_memcpy
    let size_ratio = std::mem::size_of::<T>() / std::mem::size_of::<u8>();
    let new_vec_len = vec_from.len() * size_ratio;
    let new_vec_capacity = vec_from.capacity() * size_ratio;
    let new_vec_ptr = vec_from.as_mut_ptr() as *mut u8;

    std::mem::forget(vec_from);

    unsafe { Vec::from_raw_parts(new_vec_ptr, new_vec_len, new_vec_capacity) }
}

fn matrix_multiply(blockSize: usize, dimsA: &[usize], dimsB: &[usize], cricket_connection: &mut Client, cubin: &Vec<u8>, iterations: usize) {

    let module = cricket_connection
        .cuda_module_load_data(cubin)
        .expect("Couldn't load cubin on remote GPU");

    println!("Getting histogram kernel function from cubin");
    let kernel = cricket_connection
        .cuda_module_get_function(&module, "MatrixMulCUDA")
        .expect(&format!(
            "failed to get function from cubin"
        ));
    // Allocate host memory for matrices A and B
    let size_a = dimsA[0] * dimsA[1];
    let mem_size_a = std::mem::size_of::<f32>() * size_a;
    let val_a = 1.0f32;
    let h_a = vec![val_a; size_a];
    let size_b = dimsB[0] * dimsB[1];
    let mem_size_b = std::mem::size_of::<f32>() * size_b;
    let val_b = 0.01f32;
    let h_b = vec![val_b; size_b];

    // Allocate host matrix C
    let size_c = dimsA[1] * dimsB[0];
    let mem_size_c = std::mem::size_of::<f32>() * size_c;
    let mut h_c_u8 = vec![0u8; mem_size_c];

    // Allocate device memory
    let mut d_a =
        black_box(cricket_connection.cuda_malloc(mem_size_a as u64)).expect("CUDA malloc failed");
    let mut d_b =
        black_box(cricket_connection.cuda_malloc(mem_size_b as u64)).expect("CUDA malloc failed");
    let d_c =
        black_box(cricket_connection.cuda_malloc(mem_size_c as u64)).expect("CUDA malloc failed");

    let stream = cricket_connection.cuda_stream_create().expect("CUDA stream creation failed");

    // Copy host memory to device
    let h_a_u8 = vec_cast(h_a);
    let h_b_u8 = vec_cast(h_b);
    black_box(
        cricket_connection
            .cuda_memcpy_htod(&h_a_u8, &mut d_a)
            .expect("Memcopy to host failed"),
    );
    black_box(
        cricket_connection
            .cuda_memcpy_htod(&h_b_u8, &mut d_b)
            .expect("Memcopy to host failed"),
    );
    

    let threads = &(blockSize as u32, blockSize as u32, 1);
    let grid = &(
        dimsB[0] as u32 / threads.0 as u32,
        dimsA[1] as u32 / threads.1 as u32,
        1,
    );

    println!("Computing result using CUDA Kernel...");

    let params = ParameterBuilder::new()
        .add_parameter(d_c.ptr())
        .add_parameter(d_a.ptr())
        .add_parameter(d_b.ptr())
        .add_parameter::<u32>(dimsA[0] as u32)
        .add_parameter::<u32>(dimsB[0] as u32)
        .build();

    black_box(
        cricket_connection
            .cuda_launch_kernel(
                &kernel,
                grid,
                threads,
                &0, // sharedMemBytes
                Some(&stream),
                &params,
            )
            .expect("Histogram kernel launch failed"),
    );
    println!("done");
    cricket_connection.cuda_get_last_error().unwrap();
    cricket_connection.cuda_device_synchronize().unwrap();
    
    let start = Instant::now();
    for _i in 0..iterations {
        black_box(
            cricket_connection
                .cuda_launch_kernel(
                    &kernel,
                    grid,
                    threads,
                    &0, // sharedMemBytes
                    Some(&stream),
                    &params,
                )
                .expect("Histogram kernel launch failed"),
        );
    }
    cricket_connection.cuda_device_synchronize().unwrap();
    let time = start.elapsed().as_millis() as f64 / 1000.0;
    println!("TIME: {time:.6}");
    
    // Copy result from device to host
    black_box(
        cricket_connection
            .cuda_memcpy_dtoh(&d_c, &mut h_c_u8)
            .expect("Memcopy to host failed"),
    );
    cricket_connection.cuda_device_synchronize().unwrap();
    
    let h_c : Vec<f32> = h_c_u8
        .chunks_exact(4)
        .map(|chunk| f32::from_le_bytes(chunk.try_into().unwrap()))
        .collect();
    
    println!("Checking computed result for correctness: ");
    let mut correct = true;
    // test relative error by the formula
    //     |<x, y>_cpu - <x,y>_gpu|/<|x|, |y|>  < eps
    let eps = 0.000001f32;

    for i in 0..size_c {
        let dot_length = dimsA[0] as f32;
        let abs_err = (h_c[i] - dot_length * val_b).abs();
        let abs_val = h_c[i].abs();
        let rel_err = abs_err / abs_val / dot_length;

        if rel_err > eps {
            correct = false;
            println!(
                "Error! Matrix[{}] = {}, ref={} error term is > {}",
                i, h_c[i], dot_length * val_b, eps
            );
        }
    }

    println!("Result = {}", if correct { "PASS" } else { "FAIL" });
    


    println!("Unloading module");
    cricket_connection
        .cuda_module_unload(module)
        .expect("failed to send rpc-request");

}


fn main() {
    let args = Cli::parse();
    let mut cubin;
    if let Some(cubin_path) = args.cubin {
        let cubin_path = Path::new(&cubin_path);

        let mut f = File::open(&cubin_path).expect(&format!("can't open file {cubin_path:?}"));
        let metadata = fs::metadata(cubin_path).expect("unable to read metadata");
        cubin = vec![0; metadata.len() as usize];
        f.read(&mut cubin).expect("buffer overflow");
    } else {
        let cubin_bytes = include_bytes!("kernels/kernels.cubin");
        cubin = Vec::with_capacity(cubin_bytes.len());
        cubin.extend_from_slice(cubin_bytes);
    }

    let mut cricket_connection = Client::connect(
        IpAddr::from_str(&args.cricket_server)
            .expect(&format!("Invalid IP Addr {}", args.cricket_server)),
    )
    .expect(&format!(
        "Failed to connect to server at {}",
        args.cricket_server
    ));
    println!("Connection to {} successful", args.cricket_server);

    println!("init CUDA");
    let _dev_cnt =
        black_box(cricket_connection.cuda_get_device_count()).expect("Couldn't get device count");

    let dimsA = [BLOCKSIZE * 2 * 5, BLOCKSIZE * 2 * 5, 1];
    let dimsB = [BLOCKSIZE * 4 * 5, BLOCKSIZE * 2 * 5, 1];
    
    let mm_start = Instant::now();

    matrix_multiply(BLOCKSIZE, &dimsA, &dimsB, &mut cricket_connection, &cubin, args.iterations);

    let mm_time = mm_start.elapsed().as_micros() as f64 / 1000000.0;
    println!("TOTALTIME: {mm_time:.6}");
}
