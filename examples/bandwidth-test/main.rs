use cricket::Client;
use std::hint::black_box;
use std::net::IpAddr;
use std::str::FromStr;
use std::time::Instant;
use std::vec::*;

use clap::{Parser, ValueEnum};

#[path = "../common/lib.rs"]
mod common;
use common::memarg_parser;

#[cfg(target_os = "hermit")]
use hermit_sys as _;

const QUICK_MODE_MEM_SIZE: &str = "512Mi";

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    #[arg(value_enum, default_value_t = MemcpyKind::ALL)]
    kind: MemcpyKind,

    /// Set GPU with device-number
    #[arg(long, default_value_t = 0)]
    device: i32,

    /// Set memcpy-size (with optional postfixes Ki,Mi,Gi)
    #[arg(long, short, default_value_t = String::from(QUICK_MODE_MEM_SIZE))]
    memsize: String,

    /// IP of the cricket server
    #[arg(long, short, default_value_t = String::from("127.0.0.1"), value_name = "IP")]
    cricket_server: String,

    /// Number of runs for each benchmark.
    #[arg(long, default_value_t = 10)]
    iterations: usize,
}

#[derive(Clone, Debug, ValueEnum)]
enum MemcpyKind {
    HTOD,
    DTOH,
    DTOD,
    ALL,
}

fn main() {
    let args = Cli::parse();

    let mut cricket_connection = Client::connect(
        IpAddr::from_str(&args.cricket_server)
            .expect(&format!("Invalid IP Addr {}", args.cricket_server)),
    )
    .expect(&format!(
        "Failed to connect to server at {}",
        args.cricket_server
    ));
    println!("Connection to {} successful", args.cricket_server);

    cricket_connection
        .cuda_set_device(args.device)
        .expect("Failed to set device:");
    let device = cricket_connection
        .cuda_get_device()
        .expect("Couldn't get device nr");
    println!("Running on Device {}", device);

    let memsize = memarg_parser(&args.memsize);

    println!(
        "Starting Benchmark {:?} with {} iterations",
        args.kind, args.iterations
    );
    match args.kind {
        MemcpyKind::HTOD => {
            let bandwidth =
                test_host_to_device_transfer(&mut cricket_connection, memsize, args.iterations);
            println!("Bandwidth HTOD: {0:.1} MiB/s", bandwidth);
        }
        MemcpyKind::DTOH => {
            let bandwidth =
                test_device_to_host_transfer(&mut cricket_connection, memsize, args.iterations);
            println!("Bandwidth DTOH: {0:.1} MiB/s", bandwidth);
        }
        MemcpyKind::DTOD => {
            let bandwidth =
                test_device_to_device_transfer(&mut cricket_connection, memsize, args.iterations);
            println!("Bandwidth DTOD: {0:.1} MiB/s", bandwidth);
        }
        MemcpyKind::ALL => {
            let bandwidth =
                test_host_to_device_transfer(&mut cricket_connection, memsize, args.iterations);
            println!("Bandwidth HTOD: {0:.1} MiB/s", bandwidth);
            let bandwidth =
                test_device_to_host_transfer(&mut cricket_connection, memsize, args.iterations);
            println!("Bandwidth DTOH: {0:.1} MiB/s", bandwidth);
            let bandwidth =
                test_device_to_device_transfer(&mut cricket_connection, memsize, args.iterations);
            println!("Bandwidth DTOD: {0:.1} MiB/s", bandwidth);
        }
    }
}

fn calculate_bandwidth(total_time_in_ms: u128, iterations: usize, memsize: u64) -> f64 {
    let time_per_copy_in_ms = total_time_in_ms as f64 / iterations as f64;
    let bandwidth_in_bytes_per_s = (memsize * 1024) as f64 / time_per_copy_in_ms;
    bandwidth_in_bytes_per_s / (1024 * 1024) as f64
}

fn test_device_to_host_transfer(
    cricket_connection: &mut Client,
    memsize: u64,
    iterations: usize,
) -> f64 {
    // allocate host memory
    let mut host_mem: Vec<u8> = Vec::with_capacity(memsize as usize);

    // initialize memory
    for i in 0..memsize {
        host_mem.push((i & 0xff) as u8);
    }

    // allocate & initialize device memory
    let mut device_mem = cricket_connection
        .cuda_malloc(memsize as u64)
        .expect("Couldn't allocate memory");
    cricket_connection
        .cuda_memcpy_htod(&host_mem, &mut device_mem)
        .expect("Memcopy to device failed");

    let mut mem = vec![0; memsize as usize];
    // Hot loop
    let time_begin = Instant::now();
    for _i in 0..iterations {
        black_box(
            cricket_connection
                .cuda_memcpy_dtoh(&device_mem, &mut mem)
                .expect("Memcopy to host failed"),
        );
    }
    let elapsed_time = time_begin.elapsed().as_millis();


    calculate_bandwidth(elapsed_time, iterations, memsize)
}

fn test_host_to_device_transfer(
    cricket_connection: &mut Client,
    memsize: u64,
    iterations: usize,
) -> f64 {
    // allocate host memory
    let mut host_mem: Vec<u8> = Vec::with_capacity(memsize as usize);

    // initialize memory
    for i in 0..memsize {
        host_mem.push((i & 0xff) as u8);
    }

    // allocate device memory
    let mut device_mem = cricket_connection
        .cuda_malloc(memsize as u64)
        .expect("Couldn't allocate memory");

    // Hot loop
    let time_begin = Instant::now();
    for _i in 0..iterations {
        black_box(
            cricket_connection
                .cuda_memcpy_htod(&host_mem, &mut device_mem)
                .expect("Failed to send RPC-Request"),
        );
    }
    let elapsed_time = time_begin.elapsed().as_millis();


    // calculate bandwidth in MiB/s
    calculate_bandwidth(elapsed_time, iterations, memsize)
}

fn test_device_to_device_transfer(
    cricket_connection: &mut Client,
    memsize: u64,
    iterations: usize,
) -> f64 {
    // allocate host memory
    let mut host_mem: Vec<u8> = Vec::with_capacity(memsize as usize);

    // initialize memory
    for i in 0..memsize {
        host_mem.push((i & 0xff) as u8);
    }

    // allocate device memory
    let mut device_mem_i = cricket_connection
        .cuda_malloc(memsize)
        .expect("Couldn't allocate i memory");
    let mut device_mem_o = cricket_connection
        .cuda_malloc(memsize)
        .expect("Couldn't allocate o memory");

    // initialize device memory
    cricket_connection
        .cuda_memcpy_htod(&host_mem, &mut device_mem_i)
        .expect("Failed to copy memory to device");

    // Iterations
    let time_begin = Instant::now();
    for _i in 0..iterations {
        black_box(
            cricket_connection
                .cuda_memcpy_dtod(&mut device_mem_o, &device_mem_i)
                .expect("Failed to copy memory device-to-device"),
        );
    }
    let elapsed_time = time_begin.elapsed().as_millis();

    // free device memory

    // calculate bandwidth in MiB/s
    calculate_bandwidth(elapsed_time, iterations, memsize)
}
