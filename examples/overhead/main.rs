#![allow(non_snake_case)]

use clap::Parser;
use cricket::{Client, ParameterBuilder};
use std::fs::{self, File};
use std::hint::black_box;
use std::io::Read;
use std::net::IpAddr;
use std::path::Path;
use std::str::FromStr;
use std::time::Instant;

#[cfg(target_os = "hermit")]
use hermit_sys as _;

#[path = "../common/lib.rs"]
mod common;
use common::memarg_parser;

const MEMSIZE: &str = "1Mi";
const BLOCKSIZE: u32 = 32;
const WARMUPS: usize = 100;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    /// Set GPU with device-number
    #[arg(long, default_value_t = 0)]
    device: i32,

    /// Set histogram input size (with optional postfixes Ki,Mi,Gi)
    #[arg(long, short, default_value_t = String::from(MEMSIZE))]
    memsize: String,

    /// IP of the cricket server
    #[arg(long, short, default_value_t = String::from("127.0.0.1"), value_name = "IP")]
    cricket_server: String,

    /// Number of runs for each benchmark.
    #[arg(long, short, default_value_t = 10)]
    iterations: usize,

    /// Path to the cubin containing the kernels
    #[arg(long)]
    cubin: Option<String>,
}

fn main() {
    let args = Cli::parse();
    let mut cubin;
    if let Some(cubin_path) = args.cubin {
        let cubin_path = Path::new(&cubin_path);

        let mut f = File::open(&cubin_path).expect(&format!("can't open file {cubin_path:?}"));
        let metadata = fs::metadata(cubin_path).expect("unable to read metadata");
        cubin = vec![0; metadata.len() as usize];
        f.read(&mut cubin).expect("buffer overflow");
    } else {
        let cubin_bytes = include_bytes!("kernels/kernels.cubin");
        cubin = Vec::with_capacity(cubin_bytes.len());
        cubin.extend_from_slice(cubin_bytes);
    }

    let mut cricket_connection = Client::connect(
        IpAddr::from_str(&args.cricket_server)
            .expect(&format!("Invalid IP Addr {}", args.cricket_server)),
    )
    .expect(&format!(
        "Failed to connect to server at {}",
        args.cricket_server
    ));
    println!("Connection to {} successful", args.cricket_server);

    println!("init CUDA");
    let _dev_cnt =
        black_box(cricket_connection.cuda_get_device_count()).expect("Couldn't get device count");

    println!("1. cudaGetDeviceCount ({} iterations)", args.iterations);
    for _i in 0..WARMUPS {
        let _dev_cnt = black_box(cricket_connection.cuda_get_device_count())
            .expect("Couldn't get device count");
    }
    let get_dev_cnt_start = Instant::now();
    for _i in 0..args.iterations {
        let _dev_cnt = black_box(cricket_connection.cuda_get_device_count())
            .expect("Couldn't get device count");
    }
    let get_dev_cnt_time = get_dev_cnt_start.elapsed().as_millis() as f64 / 1000.0;
    println!("TOTALTIME: {get_dev_cnt_time:.6}");

    // Set Device
    cricket_connection
        .cuda_set_device(0)
        .expect("Failed to set device:");

    // Print device info
    let device = cricket_connection
        .cuda_get_device()
        .expect("Couldn't get device nr");
    println!("Running on Device {}", device);

    let memsize = memarg_parser(&args.memsize);

    println!("2. cudaMalloc/cudaFree ({} iterations)\n", args.iterations);
    for _i in 0..WARMUPS {
        let _dev_mem_a =
            black_box(cricket_connection.cuda_malloc(memsize)).expect("CUDA malloc failed");
    }
    let malloc_free_start = Instant::now();
    for _i in 0..args.iterations {
        let _dev_mem_a =
            black_box(cricket_connection.cuda_malloc(memsize)).expect("CUDA malloc failed");
    }
    cricket_connection
        .cuda_device_synchronize()
        .expect("CUDA device synchronization failed");
    let malloc_free_time = malloc_free_start.elapsed().as_millis() as f64 / 1000.0;
    println!("TOTALTIME: {malloc_free_time:.6}");

    println!(
        "3. kernel launch w/o parameteter ({} iterations)",
        args.iterations
    );

    let module = cricket_connection
        .cuda_module_load_data(&cubin)
        .expect("Couldn't load cubin to remote GPU");

    println!("Getting kernel function `kernel_no_param` from cubin");
    let kernel_no_param = cricket_connection
        .cuda_module_get_function(&module, "kernel_no_param")
        .expect(&format!(
            "failed to get function `kernel_no_param` from cubin"
        ));

    let params = ParameterBuilder::new().build();
    for _i in 0..WARMUPS {
        black_box(
            cricket_connection
                .cuda_launch_kernel(
                    &kernel_no_param,
                    &(1, 1, 1),
                    &(BLOCKSIZE, 1, 1),
                    &0,
                    None,
                    &params,
                )
                .expect("CUDA kernel launch failed"),
        );
    }
    let kernel_launch_start = Instant::now();
    for _i in 0..args.iterations {
        black_box(
            cricket_connection
                .cuda_launch_kernel(
                    &kernel_no_param,
                    &(1, 1, 1),
                    &(BLOCKSIZE, 1, 1),
                    &0,
                    None,
                    &params,
                )
                .expect("CUDA kernel launch failed"),
        );
    }
    cricket_connection
        .cuda_device_synchronize()
        .expect("CUDA device synchronization failed");
    let kernel_launch_time = kernel_launch_start.elapsed().as_millis() as f64 / 1000.0;
    let result = cricket_connection
        .cuda_get_last_error()
        .expect("Coudln't get last CUDA error");
    println!("\nresult: {result:?}");
    println!("TOTALTIME: {kernel_launch_time:.6}");

    println!(
        "4. kernel launch w/ parameteter ({} iterations)",
        args.iterations
    );
    println!("Getting kernel function from cubin");
    let kernel = cricket_connection
        .cuda_module_get_function(&module, "kernel")
        .expect(&format!("failed to get function `kernel` from cubin"));

    let dev_x = cricket_connection
        .cuda_malloc(memsize)
        .expect("couldn't allocate memory for dev_x");
    let dev_res = cricket_connection
        .cuda_malloc(memsize)
        .expect("couldn't allocate memory for dev_res");
    let dev_A = cricket_connection
        .cuda_malloc(memsize)
        .expect("couldn't allocate memory for dev_A");

    let params = ParameterBuilder::new()
        .add_parameter(dev_A)
        .add_parameter(dev_x)
        .add_parameter(dev_res)
        .add_parameter::<u32>(0)
        .add_parameter::<u32>(0)
        .add_parameter::<u32>(0)
        .add_parameter::<u32>(0)
        .build();

    for _i in 0..WARMUPS {
        black_box(
            cricket_connection
                .cuda_launch_kernel(
                    &kernel,
                    &(1, 1, 1),
                    &(BLOCKSIZE, 1, 1),
                    &0,
                    None,
                    &params,
                )
                .expect("CUDA kernel launch failed"),
        );
    }
    let kernel_launch_start2 = Instant::now();
    for _i in 0..args.iterations {
        black_box(
            cricket_connection
                .cuda_launch_kernel(
                    &kernel,
                    &(1, 1, 1),
                    &(BLOCKSIZE, 1, 1),
                    &0,
                    None,
                    &params,
                )
                .expect("CUDA kernel launch failed"),
        );
    }
    cricket_connection
        .cuda_device_synchronize()
        .expect("CUDA device synchronization failed");
    let kernel_launch_time = kernel_launch_start2.elapsed().as_micros() as f64 / 1000000.0;
    let result = cricket_connection
        .cuda_get_last_error()
        .expect("Coudln't get last CUDA error");
    println!("\nresult: {result:?}");
    println!("TOTALTIME: {kernel_launch_time:.6}");
}
