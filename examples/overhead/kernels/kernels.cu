#include <stdint.h>
extern "C" {

__global__ void kernel(uint16_t *A, uint16_t *x, uint16_t *res, char b, short c, int a, long long int d)
{
    int i = threadIdx.x;
    res[i] = A[i] * x[i];
}

__global__
void kernel_no_param(void)
{
}
}
