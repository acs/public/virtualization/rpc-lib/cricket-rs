use clap::{Parser, ValueEnum};
use cricket::{Client, CudaSlice, ParameterBuilder};
use rand::rngs::ThreadRng;
use rand::Fill;
use std::fs::{self, File};
use std::hint::black_box;
use std::io::Read;
use std::mem::size_of;
use std::net::IpAddr;
use std::path::Path;
use std::str::FromStr;
use std::time::Instant;

#[cfg(target_os = "hermit")]
use hermit_sys as _;

#[path = "../common/lib.rs"]
mod common;
use common::memarg_parser;

const WARP_SIZE: u32 = 1 << 5;
const WARP_COUNT: u32 = 6;
const BYTE_COUNT: &str = "4Mi";
const HISTOGRAM64_BIN_COUNT: usize = 64;
const HISTOGRAM256_BIN_COUNT: usize = 256;
const SHARED_MEMORY_BANKS: u32 = 16;
const HISTOGRAM64_THREADBLOCK_SIZE: u32 = 4 * SHARED_MEMORY_BANKS;
const HISTOGRAM256_THREADBLOCK_SIZE: u32 = WARP_SIZE * WARP_COUNT;
// Note: The C-Code uses 240 partial histograms for some reason
//const PARTIAL_HISTOGRAM256_COUNT: u32 = 240;
const MERGE_THREADBLOCK_SIZE: u32 = 256;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    /// Set GPU with device-number
    #[arg(long, default_value_t = 0)]
    device: i32,

    /// Set histogram input size (with optional postfixes Ki,Mi,Gi)
    #[arg(long, short, default_value_t = String::from(BYTE_COUNT))]
    memsize: String,

    /// IP of the cricket server
    #[arg(long, short, default_value_t = String::from("127.0.0.1"), value_name = "IP")]
    cricket_server: String,

    /// Number of runs for each benchmark.
    #[arg(long, short, default_value_t = 16)]
    iterations: usize,

    /// Path to the kernels
    #[arg(long)]
    cubin_path: Option<String>,
}

#[derive(Clone, Debug, ValueEnum)]
enum MemcpyKind {
    HTOD,
    DTOH,
    DTOD,
    ALL,
}

fn main() {
    let args = Cli::parse();
    let (mut cubin64, mut cubin256);
    if let Some(cubin_path) = args.cubin_path {
        let cubin_dir = Path::new(&cubin_path);

        // read cubin64
        let cubin64_path = cubin_dir.join("histogram64.cubin");
        let mut f = File::open(&cubin64_path).expect(&format!("can't open file {cubin64_path:?}"));
        let metadata = fs::metadata(cubin64_path.clone()).expect("unable to read metadata");
        cubin64 = vec![0; metadata.len() as usize];
        f.read(&mut cubin64).expect("buffer overflow");

        // read cubin256
        let cubin256_path = cubin_dir.join("histogram256.cubin");
        let mut f = File::open(&cubin256_path).expect(&format!("can't open file {cubin64_path:?}"));
        let metadata = fs::metadata(cubin256_path).expect("unable to read metadata");
        cubin256 = vec![0; metadata.len() as usize];
        f.read(&mut cubin256).expect("buffer overflow");
    } else {
        let cubin64_bytes = include_bytes!("kernels/histogram64.cubin");
        cubin64 = Vec::with_capacity(cubin64_bytes.len());
        cubin64.extend_from_slice(cubin64_bytes);
        let cubin256_bytes = include_bytes!("kernels/histogram256.cubin");
        cubin256 = Vec::with_capacity(cubin256_bytes.len());
        cubin256.extend_from_slice(cubin256_bytes);
    }

    let mut cricket_connection = Client::connect(
        IpAddr::from_str(&args.cricket_server)
            .expect(&format!("Invalid IP Addr {}", args.cricket_server)),
    )
    .expect(&format!(
        "Failed to connect to server at {}",
        args.cricket_server
    ));
    println!("Connection to {} successful", args.cricket_server);

    // Set Device
    cricket_connection
        .cuda_set_device(0)
        .expect("Failed to set device:");

    // Print device info
    let device = cricket_connection
        .cuda_get_device()
        .expect("Couldn't get device nr");
    println!("Running on Device {}", device);

    let memsize = memarg_parser(&args.memsize);

    let mut data_host = vec![0_u8; memsize as usize];

    // generate input data
    println!("Generating input data");
    data_host.try_fill(&mut ThreadRng::default()).unwrap();
    let total_time = Instant::now();

    // Allocate Memory
    println!("Allocate memory (for data) on Device");
    let mut data_dev_mem = cricket_connection
        .cuda_malloc(memsize)
        .expect("Couldn't allocate memory");

    // Copy Data to Device
    cricket_connection
        .cuda_memcpy_htod(&data_host, &mut data_dev_mem)
        .expect("failed to send rpc-request");

    let histogram64_gpu = histogram_gpu(
        &data_dev_mem,
        &mut cricket_connection,
        &cubin64,
        "histogram64Kernel",
        "mergeHistogram64Kernel",
        HISTOGRAM64_THREADBLOCK_SIZE,
        HISTOGRAM64_BIN_COUNT,
        (memsize / (4 * size_of::<u32>() as u64))
            .try_into()
            .unwrap(),
        args.iterations,
    );

    let histogram256_gpu = histogram_gpu(
        &data_dev_mem,
        &mut cricket_connection,
        &cubin256,
        "histogram256Kernel",
        "mergeHistogram256Kernel",
        HISTOGRAM256_THREADBLOCK_SIZE,
        HISTOGRAM256_BIN_COUNT,
        (memsize / (size_of::<u32>() as u64)).try_into().unwrap(),
        args.iterations,
    );

    println!(" ...comparing the results");
    let histogram64_cpu = histogram_cpu(&data_host, HISTOGRAM64_BIN_COUNT);
    assert_eq!(
        histogram64_gpu, histogram64_cpu,
        "GPU and CPU versions of histogram are not the same"
    );

    let histogram256_cpu = histogram_cpu(&data_host, HISTOGRAM256_BIN_COUNT);
    assert_eq!(
        histogram256_gpu, histogram256_cpu,
        "GPU and CPU versions of histogram are not the same"
    );
    let total_time = total_time.elapsed().as_millis() as f64/1000.0;
    println!("TOTALTIME : {total_time} s");

    println!("Success!");
}

fn snap_down(a: u32, b: u32) -> u32 {
    a - a % b
}

fn div_up(a: u32, b: u32) -> u32 {
    if a % b != 0 {
        a / b + 1
    } else {
        a / b
    }
}

fn histogram_cpu(data: &[u8], bin_count: usize) -> Vec<u32> {
    let histogram_cpu = vec![0_u32; bin_count];
    data.iter().fold(histogram_cpu, |mut histogram_cpu, byte| {
        let bin = *byte as usize / ((u8::MAX as usize + 1) / bin_count);
        *histogram_cpu.get_mut(bin).unwrap() += 1;
        histogram_cpu
    })
}

fn histogram_gpu(
    data_dev_mem: &CudaSlice,
    cricket_connection: &mut Client,
    cubin: &[u8],
    kernel_name: &str,
    kernel_fn_name: &str,
    threadblock_size: u32,
    bin_count: usize,
    byte_count: u32,
    iterations: usize,
) -> Vec<u32> {
    let module = cricket_connection
        .cuda_module_load_data(cubin)
        .expect("Couldn't load cubin on remote GPU");

    println!("Getting histogram kernel function from cubin");
    let histogram_kernel = cricket_connection
        .cuda_module_get_function(&module, kernel_name)
        .expect(&format!(
            "failed to get function {kernel_fn_name} from cubin"
        ));

    println!("Getting merge function from cubin");
    let merge_kernel = cricket_connection
        .cuda_module_get_function(&module, kernel_fn_name)
        .expect("failed to send RPC-request");

    println!("Starting up histogram...\n");
    println!("Allocate memory (for partial histograms) on Device");
    let partial_histograms_dev = cricket_connection
        .cuda_malloc((bin_count * size_of::<u32>()) as u64)
        .expect("failed to allocate memory for parital histograms");

    println!("Allocate memory (for histogram) on Device");
    let histogram_dev_mem = cricket_connection
        .cuda_malloc((bin_count * size_of::<u32>()) as u64)
        .expect("failed to send RPC-Request");

    let histogram_count = div_up(
        data_dev_mem.len().try_into().unwrap(),
        threadblock_size * snap_down(255, size_of::<u32>() as u32),
    );

    println!(
        "Running {bin_count}-bin GPU histogram for {} bytes ({iterations} runs)...\n",
        data_dev_mem.len()
    );

    if let Err(e) = cricket_connection.cuda_get_last_error() {
        println!("WARNING: CUDA reported an error {e:?}, but no kernel was called yet");
    }

    let mut start_time = Instant::now(); // Initialized here because the compiler can't tell it will be
                                         // initialized later
    for i in 0..iterations + 1 {
        // First run is a warmup run
        if i == 1 {
            cricket_connection
                .cuda_device_synchronize()
                .expect("failed to send rpc_request");
            cricket_connection.cuda_get_last_error().unwrap();
            start_time = Instant::now();
        }

        let params = ParameterBuilder::new()
            .add_parameter(partial_histograms_dev.ptr())
            .add_parameter(data_dev_mem.ptr())
            .add_parameter::<u32>(byte_count)
            .build();

        black_box(
            cricket_connection
                .cuda_launch_kernel(
                    &histogram_kernel,
                    &(histogram_count, 1, 1),
                    &(threadblock_size, 1, 1),
                    &0, // sharedMemBytes
                    None, //CU_STREAM_PER_THREAD
                    &params,
                )
                .expect("Histogram kernel launch failed"),
        );
        cricket_connection.cuda_get_last_error().unwrap();

        // merge partial histograms
        let params = ParameterBuilder::new()
            .add_parameter(histogram_dev_mem.ptr())
            .add_parameter(partial_histograms_dev.ptr())
            .add_parameter(histogram_count)
            .build();

        black_box(
            cricket_connection
                .cuda_launch_kernel(
                    &merge_kernel,
                    &(bin_count as u32, 1, 1),
                    &(MERGE_THREADBLOCK_SIZE, 1, 1),
                    &0, // sharedMemBytes
                    None, //CU_STREAM_PER_THREAD
                    &params,
                )
                .expect("Merge kernel launch function failed"),
        );
        cricket_connection.cuda_get_last_error().unwrap();
    }
    cricket_connection
        .cuda_device_synchronize()
        .expect("failed to synchronize");

    let elapsed_time = start_time.elapsed().as_micros() / iterations as u128;
    println!("Took {elapsed_time} microseconds");

    println!("Unloading module");
    cricket_connection
        .cuda_module_unload(module)
        .expect("failed to send rpc-request");

    print!("Shutting down {bin_count}-bin histogram...\n\n");

    let mut results = vec![0; histogram_dev_mem.len() as usize];
    println!("...reading back GPU results");
    cricket_connection
        .cuda_memcpy_dtoh(&histogram_dev_mem, &mut results)
        .expect("failed to copy from device");
    let res = results
        .chunks_exact(4)
        .map(|chunk| u32::from_le_bytes(chunk.try_into().unwrap()))
        .collect();

    res
}
