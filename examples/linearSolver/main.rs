// Copyright 2022 Philipp Fensch
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use clap::Parser;
use cricket::{Client, CublasOp,  CudaSlice, DnHandle};
use std::hint::black_box;
use std::net::IpAddr;
use std::str::FromStr;
use std::time::Instant;
use std::{
    fs::File,
    io::{BufRead, Read},
};

#[cfg(target_os = "hermit")]
use hermit_sys as _;

#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
struct Cli {
    /// Set Solver: available: lu (LU-factorization), chol (Cholesky-factorization),
    /// qr (QR-factorization)
    #[clap(short, long, default_value = "lu")]
    solver: String,

    /// Set GPU with device-number
    #[arg(long, default_value_t = 0)]
    device: i32,

    /// IP of the cricket server
    #[arg(long, short, default_value_t = String::from("127.0.0.1"), value_name = "IP")]
    cricket_server: String,

    /// Set filename containing matrix in MM-format
    #[clap(long, short)]
    file: Option<String>,
}

fn main() {
    let args = Cli::parse();

    let mut cricket_con = Client::connect(
        IpAddr::from_str(&args.cricket_server)
            .expect(&format!("Invalid IP Addr {}", args.cricket_server)),
    )
    .expect(&format!(
        "Failed to connect to server at {}",
        args.cricket_server
    ));
    println!("Connection to {} successful", args.cricket_server);

    linear_solver(&mut cricket_con, args);
}

fn vec_norminf(vector: &Vec<f64>) -> f64 {
    let mut norm: f64 = 0.0;
    for i in 0..vector.len() {
        norm = norm.max(vector[i].abs());
    }
    return norm;
}

fn mat_norminf(matrix: &Vec<f64>, size: usize) -> f64 {
    let mut norm: f64 = 0.0;
    for i in 0..size {
        let mut sum = 0.0;
        for j in 0..size {
            sum += matrix[i * size + j].abs();
        }
        norm = norm.max(sum);
    }
    return norm;
}

fn linear_solver(cricket_con: &mut Client, args: Cli) {
    // Read Matrix
    println!("step 1 & 2: read matrix market format to dense matrix");
    let matrix_a = match args.file {
        Some(file) => {
            read_matrix(File::open(&file).unwrap()).expect("Failed to read file with matrix")
        }
        None => read_matrix(include_bytes!("gr_900_900_crg.mtx").as_slice()).unwrap(),
    };

    let size = (matrix_a.len() as f64).sqrt() as usize;

    println!("step 3: set right hand side vector (b) to 1");
    // Create rhs-vector
    let mut vector_b: Vec<f64> = Vec::new();
    for _ in 0..size {
        vector_b.push(1.0);
    }

    let mut handle = cricket_con
        .cusolver_dn_create()
        .expect("Couldn't initialize DN solver");

    let mut cublas_handle = cricket_con
        .cublas_create()
        .expect("Couldn't create cublas connection");

    let stream_handle = cricket_con
        .cuda_stream_create()
        .expect("Couldn't create CUDA stream");

    cricket_con
        .cusolver_dn_set_stream(&mut handle, &stream_handle)
        .expect("set_stream failed");

    cricket_con
        .cublas_set_stream(&mut cublas_handle, &stream_handle)
        .expect("Rpc-call failed");

    // Allocate memory
    let mut d_matrix_a = cricket_con
        .cuda_malloc((size * size * std::mem::size_of::<f64>()) as u64)
        .expect("CUDA memcopy of d_matrix_a failed");
    let mut d_vector_x = cricket_con
        .cuda_malloc((size * std::mem::size_of::<f64>()) as u64)
        .expect("CUDA memcopy of d_vector_x failed");
    let mut d_vector_b = cricket_con
        .cuda_malloc((size * std::mem::size_of::<f64>()) as u64)
        .expect("CUDA memcopy of d_vector_b failed");
    let mut d_vector_r = cricket_con
        .cuda_malloc((size * std::mem::size_of::<f64>()) as u64)
        .expect("CUDA memcopy of d_vector_r failed");

    println!("step 4: prepare data on device");

    // Copy matrix & rhs-vector to device
    let matrix_a_u8 = vec_cast(matrix_a);
    let vector_b_u8 = vec_cast(vector_b);
    cricket_con
        .cuda_memcpy_htod(&matrix_a_u8, &mut d_matrix_a)
        .expect("Rpc-call failed");

    cricket_con
        .cuda_memcpy_htod(&vector_b_u8, &mut d_vector_b)
        .expect("Rpc-call failed");

    let matrix_a = vec_cast_back(matrix_a_u8);
    println!("step 5: solve A*x = b");

    let mut x_inf = 0.0;
    let mut r_inf = 0.0;
    let mut a_inf = 0.0;
    let mut vector_x: Vec<f64> = Vec::new();
    let time_begin = std::time::Instant::now();
    for _i in 0..1000 {
        match args.solver.as_str() {
            "chol" => panic!("Cholesky not implemented yet"),
            "qr" => panic!("QR not implemented yet"),
            "lu" => linear_solver_lu(
                cricket_con,
                &mut handle,
                size as i32,
                &mut d_matrix_a,
                &mut d_vector_b,
                &mut d_vector_x,
            ),
            _ => panic!("Unknown solver-type"),
        }

        cricket_con
            .cuda_memcpy_dtod(&mut d_vector_r, &d_vector_b)
            .expect("Memcpy DtoD failed");

        cricket_con
            .cublas_Dgemm(
                &cublas_handle,
                CublasOp::N,
                CublasOp::N,
                size as i32,
                1, // #right-hand-sides
                size as i32,
                -1.0,
                &d_matrix_a,
                size as i32,
                &d_vector_x,
                size as i32,
                1.0,
                &mut d_vector_r,
                size as i32,
            )
            .expect("cublas_Dgemm-call failed");

        // Copy lhs-vector back
        vector_x = vec_cast_back({
            let mut h_vector_x = vec![0; size * std::mem::size_of::<f64>()];
            cricket_con
                .cuda_memcpy_dtoh(&d_vector_x, &mut h_vector_x)
                .expect("memcopy back of vector_x failed");
            h_vector_x
        });
        let vector_r = vec_cast_back({
            let mut h_vector_r = vec![0; size * std::mem::size_of::<f64>()];
            cricket_con
                .cuda_memcpy_dtoh(&d_vector_r, &mut h_vector_r)
                .expect("memcopy back of vector_r failed");
            h_vector_r
        });
        x_inf = vec_norminf(&vector_x);
        r_inf = vec_norminf(&vector_r);
        a_inf = mat_norminf(&matrix_a, size);
    }

    println!("|b - A*x| = {:.6E}", r_inf);
    println!("|A| = {:.6E}", a_inf);
    println!("|x| = {:.6E}", x_inf);
    println!("|b - A*x|/(|A|*|x|) = {:.6E}", r_inf / (a_inf * x_inf));
    // TODO: measure before println!
    let elapsed_time = time_begin.elapsed().as_micros();
    println!("Time taken {}", (elapsed_time as f64) / (1000. * 1000.));

    // Check solution
    check_solution(&matrix_a, &vector_x);
}

fn linear_solver_lu(
    cricket_con: &mut Client,
    handle: &mut DnHandle,
    size: i32,
    matrix_a_copy: &mut CudaSlice,
    vector_b: &mut CudaSlice,
    vector_x: &mut CudaSlice,
) {
    let workspace_size = cricket_con
        .cusolver_dn_dgetrf_buffer_size(
            handle,
            size as i32,
            size as i32,
            &matrix_a_copy,
            size as i32,
        )
        .expect("DN Dgetrf failed");

    let mut info = cricket_con
        .cuda_malloc(std::mem::size_of::<i32>() as u64)
        .expect("CUDA malloc for info failed");
    let mut workspace = cricket_con
        .cuda_malloc(((workspace_size as usize) * std::mem::size_of::<f64>()) as u64)
        .expect("CUDA malloc for workspace failed");
    let mut matrix_a = cricket_con
        .cuda_malloc((((size * size) as usize) * std::mem::size_of::<f64>()) as u64)
        .expect("CUDA malloc for matrix_a failed");
    let mut pivot = cricket_con
        .cuda_malloc((size as usize * std::mem::size_of::<i32>()) as u64)
        .expect("CUDA malloc for pivod failed");

    cricket_con
        .cuda_memcpy_dtod(&mut matrix_a, &matrix_a_copy)
        .expect("Memcpy DtoD failed");

    // LU-factorization
    cricket_con
        .cusolver_dn_dgetrf(
            handle,
            size as i32,
            size as i32,
            &mut matrix_a,
            size as i32,
            &mut workspace,
            &mut pivot,
            &mut info,
        )
        .expect("DN dgetrf failed");
    let h_errc = i32::from_be_bytes({
        let mut h_errc_bytes = [0; 4];
        cricket_con
            .cuda_memcpy_dtoh(&info, &mut h_errc_bytes)
            .expect("Copy back of h_errc failed");
        h_errc_bytes.try_into().unwrap()
    });
    assert!(h_errc == 0, "Failed to solve system (code: {})", h_errc);

    cricket_con
        .cuda_memcpy_dtod(vector_x, &vector_b)
        .expect("memcpy_dtod failed");

    // Solve system
    cricket_con
        .cusolver_dn_dgetrs(
            handle,
            CublasOp::N, // CUBLAS_OP_N
            size as i32,
            1, // #right-hand-sides
            &matrix_a,
            size as i32,
            &pivot,
            vector_x,
            size as i32,
            &mut info,
        )
        .expect("failed to solve system");

    cricket_con
        .cuda_device_synchronize()
        .expect("synchronize failed");
    let h_errc = i32::from_be_bytes({
        let mut h_errc_bytes = [0; 4];
        cricket_con
            .cuda_memcpy_dtoh(&info, &mut h_errc_bytes)
            .expect("Copy back of h_errc failed");
        h_errc_bytes.try_into().unwrap()
    });
    assert!(h_errc == 0, "Failed to solve system (code: {})", h_errc);
}

fn check_solution(matrix: &[f64], vector: &[f64]) {
    let size = vector.len();
    assert!(
        matrix.len() == size * size,
        "check_solution: matrix or vector wrong size!"
    );

    for i in 0..size {
        let mut row = 0.0;
        for j in 0..size {
            row += matrix[i * size + j] * vector[j]
        }
        assert!(
            row - 1.0 < 0.001,
            "Solution wrong: Row #{} (1-based)",
            i + 1
        );
    }
}

// TODO: unsafe transmute shenanigans
fn vec_cast_back<T>(mut vec_from: Vec<u8>) -> Vec<T> {
    // Convert Vec<u8> to any vector
    let size_ratio = std::mem::size_of::<T>() / std::mem::size_of::<u8>();
    assert!(vec_from.len() % size_ratio == 0, "Vec can't be casted");
    assert!(vec_from.capacity() % size_ratio == 0, "Vec can't be casted");
    let new_vec_len = vec_from.len() / size_ratio;
    let new_vec_capacity = vec_from.capacity() / size_ratio;
    let new_vec_ptr = vec_from.as_mut_ptr() as *mut T;

    std::mem::forget(vec_from);

    unsafe { Vec::from_raw_parts(new_vec_ptr, new_vec_len, new_vec_capacity) }
}

fn vec_cast<T>(mut vec_from: Vec<T>) -> Vec<u8> {
    // Convert any vector to Vec<u8> for cuda_memcpy
    let size_ratio = std::mem::size_of::<T>() / std::mem::size_of::<u8>();
    let new_vec_len = vec_from.len() * size_ratio;
    let new_vec_capacity = vec_from.capacity() * size_ratio;
    let new_vec_ptr = vec_from.as_mut_ptr() as *mut u8;

    std::mem::forget(vec_from);

    unsafe { Vec::from_raw_parts(new_vec_ptr, new_vec_len, new_vec_capacity) }
}

fn read_matrix(reader: impl Read) -> std::io::Result<std::vec::Vec<f64>> {
    let mut reader = std::io::BufReader::new(reader);
    let mut line = String::with_capacity(1024);

    // Header
    reader.read_line(&mut line).unwrap();
    if line != "%%MatrixMarket matrix coordinate real symmetric\n" {
        return Err(std::io::Error::new(
            std::io::ErrorKind::Other,
            std::format!("Wrong format: {}", line),
        ));
    }
    while line.starts_with('%') {
        line.clear();
        reader.read_line(&mut line)?;
    }
    // Size of matrix
    let mut r_c_s = line.split_whitespace();
    let rowcount = r_c_s.next().unwrap().parse::<usize>().unwrap();
    let colcount = r_c_s.next().unwrap().parse::<usize>().unwrap();
    let value_count = r_c_s.next().unwrap().parse::<usize>().unwrap();
    let mut vec: Vec<f64> = Vec::with_capacity(rowcount * colcount);
    for _ in 0..rowcount * colcount {
        vec.push(0.0);
    }

    // Parse entries
    for _i in 0..value_count {
        line.clear();
        reader.read_line(&mut line)?;
        let mut r_c_val = line.split_whitespace();
        let row = r_c_val.next().unwrap().parse::<usize>().unwrap();
        let col = r_c_val.next().unwrap().parse::<usize>().unwrap();
        let value = r_c_val.next().unwrap().parse::<f64>().unwrap();

        vec[(row - 1) * colcount + (col - 1)] = value;
        vec[(col - 1) * colcount + (row - 1)] = value;
    }
    Ok(vec)
}
