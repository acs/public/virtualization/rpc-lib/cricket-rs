use std::str::FromStr;

pub fn memarg_parser(s: &str) -> u64 {
    if s.len() == 0 {
        panic!("Invalid memsize argument");
    }
    match &s[s.len() - 2..] {
        "Ki" => u64::from_str(&s[..s.len() - 2]).unwrap() * 1024,
        "Mi" => u64::from_str(&s[..s.len() - 2]).unwrap() * 1024 * 1024,
        "Gi" => u64::from_str(&s[..s.len() - 2]).unwrap() * 1024 * 1024 * 1024,
        _ => u64::from_str(&s).expect(""),
    }
}
