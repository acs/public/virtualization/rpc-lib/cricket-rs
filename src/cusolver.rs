use crate::cuda_types::CudaError;
use crate::{
    check_code, int_result, ptr_result, Client, CublasOp, CudaSlice, RequestError, RequestResult,
    StreamHandle,
};

pub struct DnHandle {
    handle: u64,
    cricket: Client,
}
impl Drop for DnHandle {
    fn drop(&mut self) {
        self.cricket
            .cusolver_dn_destroy(self.handle)
            .unwrap_or_else(|e| println!("Error: Can't destroy DN solver {}. Reason: {e:?}", self.handle))
    }
}

impl Client {
    pub fn cusolver_dn_create(&mut self) -> RequestResult<DnHandle> {
        let cricket = self.clone();
        match self.inner().rpc_cusolverDnCreate()? {
            ptr_result::Case0 { ptr } => Ok(DnHandle {
                handle: ptr,
                cricket,
            }),
            ptr_result::CaseDefault(cuda_code) => Err(RequestError::CudaErr(
                CudaError::try_from(cuda_code).expect("Invalid CUDA response {cuda_code}"),
            )),
        }
    }

    pub fn cusolver_dn_destroy(&mut self, handle: u64) -> RequestResult<()> {
        check_code(self.inner().rpc_cusolverDnDestroy(&handle)?)
    }

    #[allow(non_snake_case)]
    pub fn cusolver_dn_set_stream(
        &mut self,
        dn_handle: &mut DnHandle,
        stream_handle: &StreamHandle,
    ) -> RequestResult<()> {
        check_code(
            self.inner()
                .rpc_cusolverDnSetStream(&dn_handle.handle, &stream_handle.handle)?,
        )
    }

    #[allow(non_snake_case)]
    pub fn cusolver_dn_dgetrf_buffer_size(
        &mut self,
        handle: &mut DnHandle,
        m: i32,
        n: i32,
        A: &CudaSlice,
        lda: i32,
    ) -> RequestResult<i32> {
        match self.inner().rpc_cusolverDnDgetrf_bufferSize(
            &handle.handle,
            &m,
            &n,
            &A.data_ptr,
            &lda,
        )? {
            int_result::Case0 { data } => Ok(data),
            int_result::CaseDefault(val) => Err(RequestError::from_cuda_code(val)),
        }
    }

    #[allow(non_snake_case)]
    pub fn cusolver_dn_dgetrf(
        &mut self,
        handle: &mut DnHandle,
        m: i32,
        n: i32,
        A: &mut CudaSlice,
        lda: i32,
        workspace: &mut CudaSlice,
        pivot: &mut CudaSlice,
        info: &mut CudaSlice,
    ) -> RequestResult<()> {
        check_code(self.inner().rpc_cusolverDnDgetrf(
            &handle.handle,
            &m,
            &n,
            &A.data_ptr,
            &lda,
            &workspace.data_ptr,
            &pivot.data_ptr,
            &info.data_ptr,
        )?)
    }

    #[allow(non_snake_case)]
    pub fn cusolver_dn_dgetrs(
        &mut self,
        handle: &mut DnHandle,
        trans: CublasOp,
        n: i32,
        nrhs: i32,
        A: &CudaSlice,
        lda: i32,
        Ipv: &CudaSlice,
        B: &mut CudaSlice,
        ldb: i32,
        info: &mut CudaSlice,
    ) -> RequestResult<()> {
        check_code(self.inner().rpc_cusolverDnDgetrs(
            &handle.handle,
            &(trans as i32),
            &n,
            &nrhs,
            &A.data_ptr,
            &lda,
            &Ipv.data_ptr,
            &B.data_ptr,
            &ldb,
            &info.data_ptr,
        )?)
    }
}
