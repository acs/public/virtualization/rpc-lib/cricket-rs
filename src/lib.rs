mod alloc;
mod cublas;
mod cuda_types;
mod cusolver;
mod memcpy;
mod parameters;
mod stream;

use cuda_types::{CudaError, CudaFunction, CudaModule};
use std::{
    fs, io,
    net::IpAddr,
    path::Path,
    sync::{Arc, Mutex, MutexGuard},
};

use thiserror::Error;

pub use crate::alloc::CudaSlice;
pub use crate::cublas::{CublasHandle, CublasOp};
pub use crate::cusolver::DnHandle;
pub use crate::stream::StreamHandle;
pub use parameters::{ParameterBuilder, Parameters};

#[rpc_lib::include_rpcl("cricket.x")]
struct RpcClient;

#[derive(Clone)]
pub struct Client {
    pub(crate) inner: Arc<Mutex<Box<RpcClient>>>,
}

#[derive(Error, Debug)]
pub enum RequestError {
    #[error(transparent)]
    Io(#[from] io::Error),
    #[error("Nonzero CUDA error code: {0:?}")]
    CudaErr(CudaError),
    #[error("Parameters for this function are incorrect")]
    InvalidCall(&'static str),
}
impl RequestError {
    fn from_cuda_code(code: i32) -> Self {
        Self::CudaErr(CudaError::try_from(code).unwrap())
    }
}

pub type RequestResult<T> = Result<T, RequestError>;

fn check_code(code: i32) -> RequestResult<()> {
    match code {
        0 => Ok(()),
        code => Err(RequestError::CudaErr(
            CudaError::try_from(code).expect("invalid CUDA response {code}"),
        )),
    }
}

impl Client {
    pub fn connect(addr: IpAddr) -> io::Result<Self> {
        let inner = Arc::new(Mutex::new(Box::new(RpcClient::new(&addr.to_string())?)));
        Ok(Self { inner })
    }

    fn inner(&mut self) -> MutexGuard<'_, Box<RpcClient>> {
        (self.inner).lock().unwrap()
    }

    pub fn cuda_set_device(&mut self, device: i32) -> RequestResult<()> {
        check_code(self.inner().CUDA_SET_DEVICE(&device)?)?;
        Ok(())
    }

    pub fn cuda_get_device(&mut self) -> RequestResult<i32> {
        match self.inner().CUDA_GET_DEVICE()? {
            int_result::Case0 { data } => Ok(data),
            int_result::CaseDefault(val) => Err(RequestError::from_cuda_code(val)),
        }
    }

    pub fn cuda_get_device_count(&mut self) -> RequestResult<i32> {
        match self.inner().CUDA_GET_DEVICE_COUNT()? {
            int_result::Case0 { data } => Ok(data),
            int_result::CaseDefault(val) => Err(RequestError::from_cuda_code(val)),
        }
    }

    pub fn cuda_device_synchronize(&mut self) -> RequestResult<()> {
        check_code(self.inner().CUDA_DEVICE_SYNCHRONIZE()?)
    }

    pub fn cuda_module_load(&mut self, cubin: &Path) -> RequestResult<CudaModule> {
        let abspath = fs::canonicalize(cubin).expect("Invalid cubin path");
        match self
            .inner()
            .rpc_cuModuleLoad(&abspath.into_os_string().into_string().unwrap())?
        {
            ptr_result::Case0 { ptr } => Ok(CudaModule::new(ptr)),
            ptr_result::CaseDefault(val) => Err(RequestError::from_cuda_code(val)),
        }
    }

    pub fn cuda_module_load_data(&mut self, cubin: &[u8]) -> RequestResult<CudaModule> {
        // TODO: let rpc_cuModuleLoadData take a &[u8] instead of a &Vec
        let v = Vec::from(cubin);
        match self.inner().rpc_cuModuleLoadData(&v)? {
            ptr_result::Case0 { ptr } => Ok(CudaModule::new(ptr)),
            ptr_result::CaseDefault(val) => Err(RequestError::from_cuda_code(val)),
        }
    }

    pub fn cuda_module_unload(&mut self, module: CudaModule) -> RequestResult<()> {
        check_code(self.inner().rpc_cuModuleUnload(&module.handle)?)
    }

    pub fn cuda_module_get_function(
        &mut self,
        module: &CudaModule,
        name: &str,
    ) -> RequestResult<CudaFunction> {
        match self
            .inner()
            .rpc_cuModuleGetFunction(&module.handle, &name.to_string())?
        {
            ptr_result::Case0 { ptr } => Ok(CudaFunction::new(ptr)),
            ptr_result::CaseDefault(val) => Err(RequestError::from_cuda_code(val)),
        }
    }

    pub fn cuda_launch_kernel(
        &mut self,
        kernel: &CudaFunction,
        grid_dim: &(u32, u32, u32),
        block_dim: &(u32, u32, u32),
        args: &u32,
        stream_id: Option<&StreamHandle>,
        parameters: &Parameters,
    ) -> RequestResult<()> {
        let stream_id = match stream_id {
            Some(handle) => handle.handle,
            None => 0,
        };
        check_code(self.inner().rpc_cuLaunchKernel(
            &kernel.handle,
            &grid_dim.0,
            &grid_dim.1,
            &grid_dim.2,
            &block_dim.0,
            &block_dim.1,
            &block_dim.2,
            args,
            &stream_id,
            parameters.inner(),
        )?)
    }

    pub fn cuda_get_last_error(&mut self) -> RequestResult<()> {
        check_code(self.inner().CUDA_GET_LAST_ERROR()?)
    }
}
