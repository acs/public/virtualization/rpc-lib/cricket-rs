use std::{
    cmp,
    io::{self, Read, Write},
    net::TcpStream,
    sync::Mutex,
    thread,
};

use crate::{
    check_code, dint, dint_result, Client, CudaSlice, RequestError, RequestResult,
};
use rpc_lib::RawResponseUnion;

impl Client {
    // FIXME: Use `src: &[u8]`
    pub fn cuda_memcpy_htod(&mut self, src: &Vec<u8>, dst: &mut CudaSlice) -> RequestResult<()> {
        if src.len() as u64 > dst.len() {
            return Err(RequestError::InvalidCall(
                "Memcopy target is smaller than source",
            ));
        };
        check_code(
            self.inner()
                .CUDA_MEMCPY_HTOD(&dst.data_ptr, src, &dst.len())?,
        )?;
        Ok(())
    }

    pub fn cuda_memcpy_dtoh(&mut self, src: &CudaSlice, dst: &mut [u8]) -> RequestResult<()> {
        if src.len() as usize != dst.len() {
            return Err(RequestError::InvalidCall(
                "Memcopy target is smaller than source",
            ));
        };

        let mut discr = 0;
        let mut data_len = 0;

        {
            let mut target = RawResponseUnion {
                data: dst,
                discriminant: &mut discr,
                data_length: &mut data_len,
            };
            self.inner()
                .CUDA_MEMCPY_DTOH_raw(&mut target, &src.data_ptr, &src.len())?;
        }
        Ok(())
    }

    pub fn cuda_memcpy_dtod(&mut self, dst: &mut CudaSlice, src: &CudaSlice) -> RequestResult<()> {
        if src.len() > dst.len() {
            return Err(RequestError::InvalidCall(
                "Memcopy target is smaller than source",
            ));
        };
        check_code(
            self.inner()
                .CUDA_MEMCPY_DTOD(&dst.data_ptr, &src.data_ptr, &src.len())?,
        )
    }

    fn parallelism() -> usize {
        cmp::min(thread::available_parallelism().unwrap().get(), 4)
    }

    pub fn cuda_memcpy_mt_htod(&mut self, src: &[u8], dst: &mut CudaSlice) -> RequestResult<()> {
        assert_eq!(src.len() as u64, dst.len());

        let thread_count = Self::parallelism();

        let dint {
            i1: port,
            i2: sync_id,
        } = match self.inner().CUDA_MEMCPY_MT_HTOD(
            &dst.data_ptr,
            &dst.len(),
            &(thread_count as i32),
        )? {
            dint_result::Case0 { data } => data,
            dint_result::CaseDefault(_code) => todo!(),
        };

        let addr = {
            let mut addr = self.inner().client.peer_addr()?;
            addr.set_port(port as u16);
            addr
        };

        thread::scope(|s| {
            let handles = (0..thread_count)
                .zip(src.chunks(src.len() / thread_count))
                .map(|(thread_num, chunk)| {
                    s.spawn(move || {
                        let mut stream = TcpStream::connect(addr)?;

                        // TODO: Use write_all_vectored once stabilized
                        // https://github.com/rust-lang/rust/issues/70436
                        stream.write_all(&(thread_num as i32).to_ne_bytes())?;
                        stream.write_all(chunk)?;

                        io::Result::Ok(())
                    })
                })
                .collect::<Vec<_>>();

            for handle in handles {
                handle.join().unwrap()?;
            }

            io::Result::Ok(())
        })?;

        check_code(self.inner().CUDA_MEMCPY_MT_SYNC(&sync_id)?)?;

        Ok(())
    }

    pub fn cuda_memcpy_mt_dtoh(&mut self, src: &CudaSlice, dst: &mut [u8]) -> RequestResult<()> {
        assert_eq!(src.len(), dst.len() as u64);

        let thread_count = Self::parallelism();

        let dint {
            i1: port,
            i2: sync_id,
        } = match self.inner().CUDA_MEMCPY_MT_DTOH(
            &src.data_ptr,
            &src.len(),
            &(thread_count as i32),
        )? {
            dint_result::Case0 { data } => data,
            dint_result::CaseDefault(_code) => todo!(),
        };

        let addr = {
            let mut addr = self.inner().client.peer_addr()?;
            addr.set_port(port as u16);
            addr
        };

        let chunks = Mutex::new(
            dst.chunks_mut(dst.len() / thread_count)
                .map(Option::Some)
                .collect::<Vec<_>>(),
        );

        thread::scope(|s| {
            let handles = (0..thread_count)
                .map(|_| {
                    let chunks = &chunks;
                    s.spawn(move || {
                        let mut stream = TcpStream::connect(addr)?;

                        let thread_num = {
                            let mut bytes = [0; 4];
                            stream.read_exact(&mut bytes)?;
                            i32::from_ne_bytes(bytes) as usize
                        };
                        let chunk = chunks.lock().unwrap()[thread_num].take().unwrap();
                        stream.read_exact(chunk)?;

                        io::Result::Ok(())
                    })
                })
                .collect::<Vec<_>>();

            for handle in handles {
                handle.join().unwrap()?;
            }

            io::Result::Ok(())
        })?;

        check_code(self.inner().CUDA_MEMCPY_MT_SYNC(&sync_id)?)?;

        Ok(())
    }
}
