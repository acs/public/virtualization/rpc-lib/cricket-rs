use crate::cuda_types::CudaError;
use crate::{check_code, ptr_result, Client, RequestError, RequestResult};

pub struct StreamHandle {
    pub(crate) handle: u64,
    cricket: Client,
}
impl Drop for StreamHandle {
    fn drop(&mut self) {
        self.cricket
            .cuda_stream_destroy(self.handle)
            .unwrap_or_else(|e| println!("Error: Can't destroy cricket stream {}. Reason: {e:?}", self.handle))
    }
}

impl Client {
    pub fn cuda_stream_create(&mut self) -> RequestResult<StreamHandle> {
        let cricket = self.clone();
        match self.inner().CUDA_STREAM_CREATE()? {
            ptr_result::Case0 { ptr } => Ok(StreamHandle {
                handle: ptr,
                cricket,
            }),
            ptr_result::CaseDefault(cuda_code) => Err(RequestError::CudaErr(
                CudaError::try_from(cuda_code).expect("Invalid CUDA response {cuda_code}"),
            )),
        }
    }

    fn cuda_stream_destroy(&mut self, handle: u64) -> RequestResult<()> {
        check_code(self.inner().CUDA_STREAM_DESTROY(&handle)?)
    }
}
