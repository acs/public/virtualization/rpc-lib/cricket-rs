use crate::cuda_types::CudaError;
use crate::{check_code, ptr_result, Client, CudaSlice, RequestError, RequestResult, StreamHandle};

pub struct CublasHandle {
    handle: u64,
    cricket: Client,
}
impl Drop for CublasHandle {
    fn drop(&mut self) {
        self.cricket
            .cublas_destroy(self.handle)
            .unwrap_or_else(|e| println!("Error: Can't destroy cublas {}. Reason: {e:?}", self.handle))
    }
}

#[repr(i32)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum CublasOp {
    N = 0,
    T = 1,
    C = 2,
}

impl Client {
    pub fn cublas_create(&mut self) -> RequestResult<CublasHandle> {
        let cricket = self.clone();
        match self.inner().rpc_cublasCreate()? {
            ptr_result::Case0 { ptr } => Ok(CublasHandle {
                handle: ptr,
                cricket,
            }),
            ptr_result::CaseDefault(cuda_code) => Err(RequestError::CudaErr(
                CudaError::try_from(cuda_code).expect("Invalid CUDA response {cuda_code}"),
            )),
        }
    }

    pub fn cublas_destroy(&mut self, handle: u64) -> RequestResult<()> {
        check_code(self.inner().rpc_cublasDestroy(&handle)?)
    }

    pub fn cublas_set_stream(
        &mut self,
        cublas_handle: &mut CublasHandle,
        stream_handle: &StreamHandle,
    ) -> RequestResult<()> {
        check_code(
            self.inner()
                .rpc_cublasSetStream(&cublas_handle.handle, &stream_handle.handle)?,
        )
    }

    #[allow(non_snake_case)]
    pub fn cublas_Dgemm(
        &mut self,
        handle: &CublasHandle,
        transa: CublasOp,
        transb: CublasOp,
        m: i32,
        n: i32,
        k: i32,
        alpha: f64,
        A: &CudaSlice,
        lda: i32,
        B: &CudaSlice,
        ldb: i32,
        beta: f64,
        C: &mut CudaSlice,
        ldc: i32,
    ) -> RequestResult<()> {
        check_code(self.inner().rpc_cublasDgemm(
            &handle.handle,
            &(transa as i32),
            &(transb as i32),
            &m,
            &n,
            &k,
            &alpha,
            &A.data_ptr,
            &lda,
            &B.data_ptr,
            &ldb,
            &beta,
            &C.data_ptr,
            &ldc,
        )?)
    }
}
