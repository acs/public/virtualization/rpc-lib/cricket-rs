use crate::cuda_types::CudaError;
use crate::parameters::ParameterConversion;
use crate::{check_code, ptr_result, Client, RequestError, RequestResult};

pub struct CudaSlice {
    pub(crate) data_ptr: u64,
    len: u64,
    cricket: Client,
}

impl CudaSlice {
    pub const fn len(&self) -> u64 {
        self.len
    }

    pub const fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// The pointer to the data on the GPU. Not a valid pointer on the host by any means.
    pub const fn ptr(&self) -> u64 {
        self.data_ptr
    }
}
impl ParameterConversion for CudaSlice {
    fn size_of() -> u16 {
        std::mem::size_of::<u64>() as u16
    }

    fn to_le_bytes(&self) -> Vec<u8> {
        self.data_ptr.to_le_bytes().to_vec()
    }
}
impl Drop for CudaSlice {
    fn drop(&mut self) {
        self.cricket
            .cuda_free(self.data_ptr)
            .unwrap_or_else(|e| println!("Error: Can't free cricket device memory at {}. Reason: {e:?}", self.data_ptr))
    }
}

impl Client {
    pub fn cuda_malloc(&mut self, size: u64) -> RequestResult<CudaSlice> {
        let cricket = self.clone();
        match self.inner().CUDA_MALLOC(&size)? {
            ptr_result::Case0 { ptr } => Ok(CudaSlice {
                data_ptr: ptr,
                len: size,
                cricket,
            }),
            ptr_result::CaseDefault(cuda_code) => Err(RequestError::CudaErr(
                CudaError::try_from(cuda_code).expect("Invalid CUDA response {cuda_code}"),
            )),
        }
    }

    fn cuda_free(&mut self, ptr: u64) -> RequestResult<()> {
        check_code(self.inner().CUDA_FREE(&ptr)?)?;
        Ok(())
    }
}
