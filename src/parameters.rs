use std::mem::size_of;
pub struct ParameterBuilder {
    parameters: Vec<u8>,
    offsets: Vec<u8>,
    num_parameters: u64,
    current_offset: u16,
}

impl ParameterBuilder {
    pub fn new() -> Self {
        ParameterBuilder {
            parameters: vec![],
            offsets: vec![],
            num_parameters: 0,
            current_offset: 0,
        }
    }

    pub fn add_parameter<T: ParameterConversion>(&mut self, parameter: T) -> &mut Self {
        self.num_parameters += 1;
        self.offsets
            .append(&mut self.current_offset.to_le_bytes().to_vec());
        self.current_offset += <T as ParameterConversion>::size_of();
        self.parameters
            .append(&mut <T as ParameterConversion>::to_le_bytes(&parameter));
        self
    }

    pub fn build(&mut self) -> Parameters {
        let mut parameters: Vec<u8> = self.num_parameters.to_le_bytes().to_vec();
        parameters.append(&mut self.offsets);
        parameters.append(&mut self.parameters);
        Parameters(parameters)
    }
}

pub struct Parameters(Vec<u8>);
impl Parameters {
    pub(crate) fn inner<'a>(&'a self) -> &'a Vec<u8> {
        &self.0
    }
}

pub trait ParameterConversion {
    fn size_of() -> u16;

    fn to_le_bytes(&self) -> Vec<u8>;
}

macro_rules! impl_ParameterConversion {
    (for $($t:ty),+) => {
        $(impl ParameterConversion for $t {
            fn size_of() -> u16 {
                size_of::<$t>() as u16
            }

            fn to_le_bytes(&self) -> Vec::<u8> {
                Self::to_le_bytes(*self).to_vec()
            }
        })*
    }
}

impl_ParameterConversion!(for u64, u32, u16, i64, i32, i16);

#[cfg(test)]
mod test {
    use crate::ParameterBuilder;
    use std::mem::size_of;
    #[test]
    fn test_valid_replacement() {
        let built_parameters = ParameterBuilder::new().add_parameter(24_u64).build();
        let mut manual_parameters = <u64>::to_le_bytes(1).to_vec(); // number of arguments
        manual_parameters.append(&mut <u16>::to_le_bytes(0).to_vec());
        manual_parameters.append(&mut <u64>::to_le_bytes(24_u64).to_vec());
        assert_eq!(built_parameters, manual_parameters);
    }

    #[test]
    fn test_inequality() {
        let built_parameters = ParameterBuilder::new()
            .add_parameter(24_u64)
            .add_parameter(13_u64)
            .build();
        let mut manual_parameters = <u64>::to_le_bytes(1).to_vec(); // number of arguments
        manual_parameters.append(&mut <u16>::to_le_bytes(0).to_vec());
        manual_parameters.append(&mut <u64>::to_le_bytes(24_u64).to_vec());
        assert_ne!(built_parameters, manual_parameters);
    }

    #[test]
    fn test_multiple_parameters() {
        let built_parameters = ParameterBuilder::new()
            .add_parameter(24_u64)
            .add_parameter(13_u32)
            .build();
        let mut manual_parameters = <u64>::to_le_bytes(2).to_vec(); // number of arguments
        manual_parameters.append(&mut <u16>::to_le_bytes(0).to_vec());
        manual_parameters.append(&mut <u16>::to_le_bytes(size_of::<u64>() as u16).to_vec());
        manual_parameters.append(&mut <u64>::to_le_bytes(24_u64).to_vec());
        manual_parameters.append(&mut <u32>::to_le_bytes(13_u32).to_vec());
        assert_eq!(built_parameters, manual_parameters);
    }
}
