# Cricket-rs

This is the Rust client library for the [GPU virtualization server Cricket](https://github.com/RWTH-ACS/cricket) based on [rpc-lib](https://github.com/RWTH-ACS/RPC-Lib).

Usage example:

```rust
use cricket::Client;

let mut cricket_connection = Client::connect(
    IpAddr::from_str("123.123.123.123").unwrap(),
)
.expect("Failed to connect to server");

println!("Connection successful");

cricket_connection
    .cuda_set_device(args.device)
    .expect("Failed to set device:");

let device = cricket_connection
    .cuda_get_device()
    .expect("Couldn't get device nr");
println!("Running on Device {}", device);
```

For more information, take a look at the [examples](./examples).