use cricket::CudaSlice;
use criterion::{
    black_box, criterion_group, criterion_main, AxisScale, BenchmarkId, Criterion,
    PlotConfiguration, Throughput,
};

fn client() -> cricket::Client {
    let mut client = cricket::Client::connect("127.0.0.1".parse().unwrap()).unwrap();
    client.cuda_set_device(0).unwrap();
    client
}

fn buf_sizes() -> impl Iterator<Item = usize> {
    (29..=29).map(|exp| 2usize.pow(exp))
}

fn buf(size: usize) -> Vec<u8> {
    (0..size).map(|i| i as u8).collect()
}

fn bench_device_count(c: &mut Criterion) {
    c.bench_function("Device Count", |b| {
        let mut client = client();

        b.iter(|| client.cuda_get_device_count())
    });
}

fn bench_memcpy<O>(
    c: &mut Criterion,
    group_name: &str,
    mut routine: impl FnMut(&mut cricket::Client, &mut Vec<u8>, &mut CudaSlice) -> O,
) {
    let mut group = c.benchmark_group(group_name);
    group.plot_config(PlotConfiguration::default().summary_scale(AxisScale::Logarithmic));

    for size in buf_sizes() {
        group.throughput(Throughput::Bytes(size as u64));
        group.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, &size| {
            let mut client = client();
            let mut host_mem = buf(size);
            let mut device_mem = client.cuda_malloc(size as u64).unwrap();
            client
                .cuda_memcpy_htod(&host_mem, &mut device_mem)
                .unwrap();

            b.iter(|| {
                routine(
                    black_box(&mut client),
                    black_box(&mut host_mem),
                    black_box(&mut device_mem),
                )
            });

            client.cuda_free(device_mem).unwrap();
        });
    }

    group.finish();
}

fn bench_htod(c: &mut Criterion) {
    bench_memcpy(c, "Host to Device", |client, src, dst| {
        client.cuda_memcpy_htod(src, dst)
    });
}

fn bench_dtoh(c: &mut Criterion) {
    bench_memcpy(c, "Device to Host", |client, _, src| {
        client.cuda_memcpy_dtoh(src)
    });
}

fn bench_mt_htod(c: &mut Criterion) {
    bench_memcpy(c, "Host to Device (MT)", |client, src, dst| {
        client.cuda_memcpy_mt_htod(src, dst)
    });
}

fn bench_mt_dtoh(c: &mut Criterion) {
    bench_memcpy(c, "Device to Host (MT)", |client, dst, src| {
        client.cuda_memcpy_mt_dtoh(src, dst)
    });
}

criterion_group!(
    benches,
    bench_device_count,
    bench_htod,
    bench_dtoh,
    bench_mt_htod,
    bench_mt_dtoh,
);
criterion_main!(benches);
